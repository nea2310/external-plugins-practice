# сделать класс Slider, который будет принимать DOM элемент и объект конфигурации и инициализировать на полученном DOM-элементе ion rangeslider с переданной конфигурацией

# нужно сделать демо страницу подобно https://nea2310.github.io/Metalamp-4th-task/index.html, подключить на ней слайдер дважды

# в панели управления пусть будут четыре инпута - для значений min, max, from, to

# при первой инициализации во всех инпутах должны отобразиться значения, с которыми плагин был проинициализирован, а при вводе нового значения в инпут слайдер должен на это отреагировать и поменяться

# также  когда у слайдера двигается ползунок from или to, значение в инпуте должно меняться